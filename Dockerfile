FROM phusion/baseimage:latest
MAINTAINER Tanmai Gopal <tanmaig@34cross.in>

ENV HOME /root
ENV NPS_VERSION 1.9.32.2
ENV NGINX_VERSION 1.6.2

# Uncomment both lines to generate ssh keys, and use phusion/baseimage style ssh-ing
# RUN /etc/my_init.d/00_regen_ssh_host_keys.sh
# RUN /usr/sbin/enable_insecure_key

#Install the basic stuff
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install wget unzip

RUN cd /
RUN wget https://github.com/pagespeed/ngx_pagespeed/archive/release-${NPS_VERSION}-beta.zip
RUN unzip release-${NPS_VERSION}-beta.zip
RUN cd ngx_pagespeed-release-1.9.32.2-beta/ && wget https://dl.google.com/dl/page-speed/psol/1.9.32.2.tar.gz && tar -xzvf 1.9.32.2.tar.gz

RUN cd / && wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && tar -xvzf nginx-${NGINX_VERSION}.tar.gz

RUN cd / && wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.36.tar.gz && tar -xvzf pcre-8.36.tar.gz

RUN cd nginx-${NGINX_VERSION}/ && ./configure --add-module=/ngx_pagespeed-release-${NPS_VERSION}-beta --with-pcre=/pcre-8.36/ && make && make install

#Set up the nginxconfs
ADD global.conf /usr/local/nginx/conf/nginx.conf
ADD runner /etc/service/nginx
